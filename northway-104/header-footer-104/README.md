
###Northway AAA
- club code: 104
- club zip: 12866
- https://www.aaa.com/stop
- dynamic header and footer are the custom element versions
- static header and footer are static html
- both versions use aaa ftp server css files

####FTP Server (using FileZilla)
- IP Address must be whitelisted for ftp user-account (per club): Contact ...TODO...
- Protocol: FTP
- Host: stager.aaa.com
- Encryption: Use explicit FTP over TLS if available
- Logon Type: Normal
- User: [username]
- Password: [password]

####Stager Content Manager
- IP Address must be whitelisted: Contact ...TODO...
- https://stager.aaa.com/services/contentmanager/signin/signInPage.xhtml
- User: [jhood104]
- Password: [password]
- Club Code: [104]

####Vendors
Discounts
- https://discounts.northway.aaa.com
- dynamic

National 
- https://www.aaa.com/autorepair
- dynamic
  - Stager Custom Menu 10
  - Stager Custom Menu 16
  

- http://www.aaa.com/services/cms/templates/index.html?page=SYCSInformation05
- dynamic
  - Stager Custom Menu 10
  - Stager Custom Menu 16

Travel
- https://secure.rezserver.com/car_rentals/?refid=5756
- static custom
- https://h5aaa104.revelex.com/app/0/cruise/0/selection.html
- dynamic
  - Stager Custom Menu 10
  - Stager Custom Menu 16
