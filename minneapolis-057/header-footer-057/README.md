
###Minneapolis AAA
- club code: 057
- club zip: 55444
- https://www.aaa.com/stop
- dynamic header and footer are the custom element versions
- static header and footer are static html
- both versions use aaa ftp server css files

####FTP Server (using FileZilla)
- IP Address must be whitelisted for ftp user-account (per club): Contact ...TODO...
- Protocol: FTP
- Host: stager.aaa.com
- Encryption: Use explicit FTP over TLS if available
- Logon Type: Normal
- User: [username]
- Password: [password]

####Stager Content Manager
- IP Address must be whitelisted: Contact ...TODO...
- https://stager.aaa.com/services/contentmanager/signin/signInPage.xhtml
- User: [aaaminneapoliswebservice]
- Password: [password]
- Club Code: [057]

####Vendors
RTJ Membership
- http://rtjminn.national.aaa.com
- dynamic
  - Ghost CMS with wss-managed theme (in this repo)
  - http://rtjminn.national.aaa.com/ghost
  - http://rtjminn-stage.national.aaa.com/ghost

Fuze FAQs
- https://www.fuzeqna.com/aaaminneapolis/ext/kbsearch.aspx
- dynamic

TST Travel
- https://minneapolis.tstllc.net/travel
- dynamic

Discounts
- https://minneapolis.discounts.aaa.com
- dynamic
  - Stager SEO Header
  - Stager SEO Bottom

National 
- https://www.aaa.com/autorepair
- dynamic
  - Stager SEO Header
  - Stager SEO Bottom
  

- http://www.aaa.com/services/cms/templates/index.html?page=SYCSInformation05
- dynamic
  - Stager Custom Menu 01
  - Stager Custom Menu 20

Cruise with Revelex
- https://h5aaa057.revelex.com/app/?clear=all
- static [revelex site hangs with dynamic header]
  - Stager T2 Cruise Booking Top
  - Stager T2 Cruise Booking Bottom
