/**
 * Created by VB on 7/12/2016.
 */

var path={
            'webServerPath'            :  'https://rtjappservv1.national.aaa.com/',
			'webServerredirectPath'    :  'https://rtjminn.national.aaa.com/',
            'ghostPath'                :  'http://rtjminn.national.aaa.com/',
			'hostname'                 :  'rtjminn.national.aaa.com',
			'serviceTimeout'           :   40000,
			'logPrintValue'            :   0,
            'loginForm'                :  'user-login',
			'clubName'                 :  'Minneapolis',
            'myAccount'                :  'my-account',
			'join'			           :  'join-form',
			'join-thankyou'            :  'join-thankyou',
			'manage-thankyou'          :  'manage-thankyou',
			'renew-thankyou'           :  'renew-thankyou',
			'quick-renew'              :  'quick-renew',
			'quick-renew-thankyou'     :  'quick-renew-thankyou',
			'returnUrl'                :  'returnurl',
			'cancelUrl'                :  'cancelurl',
			'offErrorPage'             :  'error-page',
            'handelBarCall'            :  'forms/getForms',
			'base'                     :  '/ghost/api/v0.1/',
			'payment_cresecure_url'    :  'https://www.sandbox-cresecure.net/hpf/1_1/?',
			'cresecure_merchandId'     :  'cre69366877SB',
			'cresecure_sessionId'	   :  '8e2b88439ab72646b544a969d899176c',
			'dataline_merchantPass'	   :  'RNG4550568',
			'dataline_form'            :  'assets/css/payment-form.html',
			'dataline_csshtml'         :  'assets/css/rcportalcss.html',
	        'handelBarKeyWords'        : ['[[login]]','[[joinmembership]]','[[myaccount]]','[[join-thankyou]]','[[manage-thankyou]]','[[quick-renew]]','[[quick-renew-thankyou]]','[[renew-thankyou]]','[[offerrorpage]]'],
			'appId'                    : 'AAACOM',
			'logoutUri'                : 'https://rtjminn.national.aaa.com/',
			'voidTranscationTime'      : 62, //comparing time for inserting new record
			'AnalyticFlag'             : 1, //0 for disable anlyatic functions for join,Manage and Renew 
		    'QuickRenew-logOut-url'    : 'https://rtjminn.national.aaa.com/' //comparing time for inserting new record
}


function includeformscripts()
{
	/*-- fix phone fields on mobile devices MUST BE LOADED 1st --*/
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/responsive-request-desktop-site.js' + "'></scr" + "ipt>");
					//if(window.location.href == path['webServerredirectPath']+path['join']+'/')
	/* form realted js*/
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/joinRenewMembership.js' + "'></scr" + "ipt>");
    document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/updatesettings.js' + "'></scr" + "ipt>");
    document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/jspdf.min.js' + "'></scr" + "ipt>");
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/autoPrint.js' + "'></scr" + "ipt>");
    document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/html2canvas.min.js' + "'></scr" + "ipt>");
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/moment.js' + "'></scr" + "ipt>");
	
	/*-- altering form content --*/
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/formarea-years.js' + "'></scr" + "ipt>");
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/formarea-associates.js' + "'></scr" + "ipt>");
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/formarea-manage-script.js' + "'></scr" + "ipt>");
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/openRequiredAreaInManage.js' + "'></scr" + "ipt>");
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/formarea-membershipStatus.js' + "'></scr" + "ipt>");
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/formarea-payment.js' + "'></scr" + "ipt>");
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/payment-page.js' + "'></scr" + "ipt>");
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/returnUrl.js' + "'></scr" + "ipt>");



	/*-- form validation --*/
    document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/jquery.validate.js' + "'></scr" + "ipt>");
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/formarea-validation.js' + "'></scr" + "ipt>");
    document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/formDynamicOfferings.js' + "'></scr" + "ipt>");
    document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/formAdminValidation.js' + "'></scr" + "ipt>");
    document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/thankyouDynamicResponse.js' + "'></scr" + "ipt>");
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/ageValidation.js' + "'></scr" + "ipt>");
    
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/jquery.maskedinput.js' + "'></scr" + "ipt>");
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/delegate.js' + "'></scr" + "ipt>");
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/jquery.creditCardValidator.js' + "'></scr" + "ipt>");
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/creditCoffeScript.js' + "'></scr" + "ipt>");

	/*-- form accoridan --*/
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/formarea-accordian-join.js' + "'></scr" + "ipt>");
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/formarea-accordian-manage.js' + "'></scr" + "ipt>");
	

	/*-- form masking --*/
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/formarea-creditcard.js' + "'></scr" + "ipt>");
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/formarea-masking.js' + "'></scr" + "ipt>");

	/*-- common functions used in other js --*/
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/commonFunctions.js' + "'></scr" + "ipt>");

	/*-- form services --*/
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/joinMembership.js' + "'></scr" + "ipt>");
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/changeMembership.js' + "'></scr" + "ipt>");
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/quickRenewMembership.js' + "'></scr" + "ipt>");
	
	/*Analytic functions*/
	document.write("<script type='text/javascript' src='" + path['webServerPath'] + 'javascripts/formJs/analytic.js' + "'></scr" + "ipt>");
	
	/*-- form files styles --*/
    document.write("<link href='"+ path['webServerPath'] + "stylesheets/formCss/formarea-styles.css'"+ 'rel="stylesheet">');
	document.write("<link href='"+ path['webServerPath'] + "stylesheets/formCss/payment-styles.css'"+ 'rel="stylesheet">');
	
}






