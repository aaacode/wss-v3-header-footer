
###Hoosier AAA
- club code: 023
- club zip: 47868
- https://www.aaa.com/stop
- dynamic header and footer are the custom element versions
- static header and footer are static html
- both versions use aaa ftp server css files

####FTP Server (using FileZilla)
- IP Address must be whitelisted for ftp user-account (per club): Contact ...TODO...
- Protocol: FTP
- Host: stager.aaa.com
- Encryption: Use explicit FTP over TLS if available
- Logon Type: Normal
- User: [username]
- Password: [password]

####Stager Content Manager
- IP Address must be whitelisted: Contact ...TODO...
- https://stager.aaa.com/services/contentmanager/signin/signInPage.xhtml
- User: [jhood023]
- Password: [password]
- Club Code: [023]

####Vendors
RTJ Membership
- https://rtjhoosier.national.aaa.com/join-form
- dynamic
  - Ghost CMS with wss-managed theme (in this repo)
  - http://rtjhoosier.national.aaa.com/ghost
  - http://rtjhoosier-stage.national.aaa.com/ghost
  - login: [connie.hawkins@montana.aaa.com]

TST Travel
- https://etravel.hoosier.aaa.com/flight
- dynamic

Discounts
- https://hoosier.discounts.aaa.com
- dynamic
  - Stager Custom Menu 10
  - Stager Custom Menu 16

National 
- https://www.aaa.com/autorepair
- dynamic
  - Stager Custom Menu 10
  - Stager Custom Menu 16


- http://www.aaa.com/services/cms/templates/index.html?page=SYCSInformation05 (todo)
- dynamic
  - Stager Custom Menu 10
  - Stager Custom Menu 16

Fuze FAQs
- https://www.fuzeqna.com/aaahoosier/ext/kbsearch.aspx
- static wssfiles
