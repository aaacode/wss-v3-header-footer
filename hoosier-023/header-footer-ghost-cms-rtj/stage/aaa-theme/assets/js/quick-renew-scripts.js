// JavaScript Document
function quickRenewPage() {
/*******************************
* DOCUMENT READY
*******************************/
"use strict";

// QUICK PURCHASE SCRIPTS //
/*******************************
* PROGRESS BAR - NEXT/PREV
*******************************/
//$('.next-panel').addClass('hidden');
$('#submit').addClass('hidden');

//counts foward and backward movement, moves through steps
var numberID  = 1;

//var formPanel = $('.sectionForm').length;
/*** NEXT BUTTON ***/
$('.next-panel').click(function() {

//	console.log('numberID '+numberID);
	$('.sectionForm').addClass('hidden');

	numberID += 1;

	$('#step-'+ numberID +'.sectionForm').removeClass('hidden').addClass('active');
//	console.log('#step-'+ numberID +'.sectionForm');

	var payment1Height = $('.payment-1').height();
	$('.payment-2 .tableBlock').height(payment1Height);
});


/*************************************
 * payment columns must be equal height
 ************************************/
$('.editButton').click(function() {
    var payment1Height = $('.payment-1').height();
    $('.payment-2 .tableBlock').height(payment1Height);
});


/*********************************************************************
* PAYMENT SECTION
*********************************************************************/

 /*************************************
 * show/hide fields when click associated EDIT button
 ************************************/
// editNone is the name for 'save mode'
$('#pay_container_address').addClass('editNone');

/************************************
 # PAYMENT SECTION FORM - save to edit mode
 ***********************************/
$(document).on("click", "#pay_edit_address", function() {
	$('#pay_container_address').removeClass('editNone');

	$('#address_bill_address1').val( $('.address_bill_address1 .userInfo').text() );
	$('#address_bill_address2').val( $('.address_bill_address2 .userInfo').text() );
	$('#address_bill_city').val( $('.address_bill_city .userInfo').text() );
	$('#address_bill_state').val( $('.address_bill_state .userInfo').text() );
	$('#address_bill_ZipCode').val( $('.address_bill_ZipCode .userInfo').text() );
});


/*******************************
* END DOCUMENT READY
*******************************/
}