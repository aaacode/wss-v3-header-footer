var analyticOfferingData = [
    {'classic_join'      : {'action':'RTJMembershipClassicJoin','eventAction':'LINKCLICK'}},
	{'basic_join'        : {'action':'RTJMembershipClassicJoin','eventAction':'LINKCLICK'}},
	{'plus_join'         : {'action':'RTJMembershipPlusJoin','eventAction':'LINKCLICK'}},
	{'rv_join'           : {'action':'RTJMembershipRVPlusJoin','eventAction':'LINKCLICK'}},
	{'premier_join'      : {'action':'RTJMembershipPremierJoin','eventAction':'LINKCLICK'}},
	{'classic_gift'      : {'action':'RTJGiftButtonClassic','eventAction':'LINKCLICK'}},
	{'basic_gift'        : {'action':'RTJGiftButtonClassic','eventAction':'LINKCLICK'}},
	{'plus_gift'         : {'action':'RTJGiftButtonPlus','eventAction':'LINKCLICK'}},
	{'rv_gift'           : {'action':'RTJGiftButtonRVPlus','eventAction':'LINKCLICK'}},
	{'premier_gift'      : {'action':'RTJGiftButtonPremier','eventAction':'LINKCLICK'}},
	{'classic_submit'    : {'action':'RTJSubmitButtonClassic','eventAction':'LINKCLICK'}},
	{'basic_submit'      : {'action':'RTJSubmitButtonClassic','eventAction':'LINKCLICK'}},
	{'plus_submit'       : {'action':'RTJSubmitButtonPlus','eventAction':'LINKCLICK'}},
	{'rv_submit'         : {'action':'RTJSubmitButtonRVPlus','eventAction':'LINKCLICK'}},
	{'premier_submit'    : {'action':'RTJSubmitButtonPremir','eventAction':'LINKCLICK'}}
]

 
var analyticParametersForOfferingOnClick = {
	"eventType"        : "RTJ",
	"category"         : "Membership",
	"subCategory"      : "Join",
	"club"             : "",
	"appId"            : "AAACOM",
	"pagetype"         : "Membership Offerings",
	"action"           : "RTJMembershipClassicJoin",
	"eventAction"      : "LINKCLICK",
}

var analyticParametersForOfferingPromocode = {
	"eventType"        : "RTJ",
	"category"         : "Membership",
	"subCategory"      : "Join",
	"club"             : "",
	"appId"            : "AAACOM",
	"pagetype"         : "Membership Offerings",
	"promocode"        : " ",
}

var analyticParametersForOfferingOnPanel = {
	"eventType"        : "RTJ",
	"category"         : "Membership",
	"subCategory"      : "Join",
	"club"             : "",
	"appId"            : "AAACOM",
	"pagetype"         : "Membership Offerings",
	"scenarioName"     : "RTJScenarioAnalysis",
	"scenarioStep"     : "RTJMembershipOffering",
	"campaignId"       : "999EbusinessAAACOMMembershipOffering"
}



var analyticParametersForDonorOnPanel = {
	"eventType"        : "RTJ",
	"category"         : "Membership",
	"subCategory"      : "Join",
	"club"             : "001",
	"appId"            : "AAACOM",
	"pagetype"         : "Donor Information",
	"scenarioName"     : "RTJScenarioAnalysis",
	"scenarioStep"     : "RTJDonorInformation",
	"campaignId"       : "999EbusinessAAACOMDonorInformation"
}


var analyticParametersForMemberOnPanel = {
	"eventType"        : "RTJ",
	"category"         : "Membership",
	"subCategory"      : "Join",
	"club"             : "",
	"appId"            : "AAACOM",
	"pagetype"         : "Member Information",
	"scenarioName"     : "RTJScenarioAnalysis",
	"scenarioStep"     : "RTJMemberInformation",
	"campaignId"       : "999EbusinessAAACOMMemberInformation"
}

var analyticParametersForAssociateMemberOnPanel = {
	"eventType"        : "RTJ",
	"category"         : "Membership",
	"subCategory"      : "Join",
	"club"             : "",
	"appId"            : "AAACOM",
	"pagetype"         : "Associate Member Information",
	"scenarioName"     : "RTJScenarioAnalysis",
	"scenarioStep"     : "RTJAssociateMemberInfo",
	"campaignId"       : "999EbusinessAAACOMAssociateMemberInfo"
}

var analyticParametersForPayemntOnClick = {
	"eventType"        : "RTJ",
	"category"         : "Membership",
	"subCategory"      : "Join",
	"club"             : "",
	"appId"            : "AAACOM",
	"pagetype"         : "Hosted Payment Page",
    "action"           : "RTJSubmitPayment ",
	"eventAction"      : "LINKCLICK",
}

var analyticParametersForPayemntOnPanel = {
	"eventType"        : "RTJ",
	"category"         : "Membership",
	"subCategory"      : "Join",
	"club"             : "",
	"appId"            : "AAACOM",
	"pagetype"         : "Hosted Payment Page",
	"scenarioName"     : "RTJScenarioAnalysis",
	"scenarioStep"     : "RTJPaymentPage",
	"campaignId"       : "999EbusinessAAACOMHostedPayment"
}

var analyticParametersForConfirmationOnPanel = {
	"eventType"        : "RTJ",
	"category"         : "Membership",
	"subCategory"      : "Join",
	"club"             : "",
	"appId"            : "AAACOM",
	"pagetype"         : "Confirmation Page",
	"scenarioName"     : "RTJScenarioAnalysis",
	"scenarioStep"     : "RTJConfirmationPage",
	"campaignId"       : "999EbusinessAAACOMRTJConfirmationPage"
}

var membershipTypesForConfirmationAnalytic = {
	"classic"        : "MBRSHP_BASIC",
	"basic"        : "MBRSHP_BASIC",
	"plus"           : "MBRSHP_PLUS",
	"premier"        : "MBRSHP_PREMIER",
}

var transcationTypesForConfirmationAnalytic = {
	"newMembership"         : "MBRSHP_NEW",
	"giftMembership"        : "MBRSHP_GIFT",
	"newAssociate"          : "MBRSHP_ADD_ASSOCIATE",
}

var analyticParametersForQuickRenewOnPanel = {
	"eventType"        : "RTJ",
	"category"         : "Membership",
	"subCategory"      : "QuickRenewJoin",
	"club"             : "",
	"appId"            : "AAACOM",
	"pagetype"         : "Quick Real Time Join Renewal",
}

var analyticParametersForQuickRenewConfirmationOnPanel = {
	"eventType"        : "RTJ",
	"category"         : "Membership",
	"subCategory"      : "QuickRenewJoin",
	"club"             : "",
	"appId"            : "AAACOM",
	"pagetype"         : "Quick Real Time Join Renewal Confirmation",
}







