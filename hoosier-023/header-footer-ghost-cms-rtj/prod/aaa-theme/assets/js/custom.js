// JavaScript Document
$(document).ready(function(){
	
// initialize FlexNav
$(".flexnav").flexNav();


/***********************************************
 * Sticky nav
 **********************************************/
$(window).scroll(function (event) {
	var height = $(window).scrollTop();
/*	console.log(height); */
	if( height > 75 ) {
		$( ".nav-wrapper").addClass('nav-to-top');
	} else {
		$( ".nav-wrapper").removeClass('nav-to-top')
	}
});


//scroll to top fucction 
$(window).scroll(function () {
	if ($(this).scrollTop() > 100) {
		$('.scrollup').fadeIn();
	} else {
		$('.scrollup').fadeOut();
	}
});

$('.scrollup').click(function () {
	$("html, body").animate({
		scrollTop: 0
	}, 600);
	return false;
});


//print function
$('.printButton').click(function() {
	//$('.table-responsive').addClass('hidden-print');
    window.print();
	
});

//end document ready	
});