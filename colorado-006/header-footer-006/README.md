
###Colorado AAA
- club code: 006
- club zip: 80222
- https://www.aaa.com/stop
- dynamic header and footer are the custom element versions
- static header and footer are static html
- both versions use aaa ftp server css files

####FTP Server (using FileZilla)
- IP Address must be whitelisted for ftp user-account (per club): Contact ...TODO...
- Protocol: FTP
- Host: stager.aaa.com
- Encryption: Use explicit FTP over TLS if available
- Logon Type: Normal
- User: [username]
- Password: [password]

####Stager Content Manager
- IP Address must be whitelisted: Contact ...TODO...
- https://stager.aaa.com/services/contentmanager/signin/signInPage.xhtml
- User: [jhood006]
- Password: [password]
- Club Code: [006]

####Vendors
Discounts
https://discounts.colorado.aaa.com/
- dynamic

National 
- https://www.aaa.com/autorepair
- dynamic
  - using Stager Custom Menu 10
  - using Stager Custom Menu 16
  

- http://www.aaa.com/services/cms/templates/index.html?page=SYCSInformation05
- dynamic
  - using Stager Custom Menu 10
  - using Stager Custom Menu 16

Vendor not known
- https://store.colorado.aaa.com

RTJ Membership
- http://rtjco.national.aaa.com
- static custom using Ghost CMS

TST Travel 
- https://travel.colorado.aaa.com/cruise
- static aaaftp
