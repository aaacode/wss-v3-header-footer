// WSS version 2019.08.22

(function () {
  var clubName = 'colorado';
  var clubNumber = '006';
  var a = '';
  var b = document.getElementsByTagName('script')[0];
  function f1(fileName) {
    a = document.createElement('script');
    a.defer = true;
    a.type = 'text/javascript';
    a.src = 'https://' + clubName + clubNumber + '.web.app/' + fileName;
    b.parentNode.insertBefore(a, b);
  }
  if(/MSIE \d|Trident.*rv:/.test(navigator.userAgent)) {
    f1('assets/polyfill.min.js');
    // f1('header/polyfill-es5.js');
    f1('header/scripts.js');
    f1('header/main-es5.js');
    // f1('footer/polyfill-es5.js');
    f1('footer/scripts.js');
    f1('footer/main-es5.js');
  } else {
    f1('header/scripts.js');
    f1('header/main-es2015.js');
    f1('footer/scripts.js');
    f1('footer/main-es2015.js');
  }
})();
