
###SouthPa AAA
- club code: 238
- club zip: 17019
- https://www.aaa.com/stop
- dynamic header and footer are the custom element versions
- static header and footer are static html
- both versions use aaa ftp server css files

####FTP Server (using FileZilla)
- IP Address must be whitelisted for ftp user-account (per club): Contact ...TODO...
- Protocol: FTP
- Host: stager.aaa.com
- Encryption: Use explicit FTP over TLS if available
- Logon Type: Normal
- User: [username]
- Password: [password]

####Stager Content Manager
- IP Address must be whitelisted: Contact ...TODO...
- https://stager.aaa.com/services/contentmanager/signin/signInPage.xhtml
- User: [jhood238]
- Password: [password]
- Club Code: [238]

####Vendors
Discounts
- https://discounts.southpa.aaa.com
- dynamic

TST Travel
- https://etravel.southpa.aaa.com/car
- dynamic

National 
- https://www.aaa.com/autorepair
- dynamic
  - Stager Custom Menu 10
  - Stager Custom Menu 16
  

- http://www.aaa.com/services/cms/templates/index.html?page=SYCSInformation05
- dynamic
  - Stager Custom Menu 10
  - Stager Custom Menu 16

Fuze FAQs
- http://www.fuzeqna.com/aaasouthpa/ext/kbsearch.aspx
- static
  - wssfiles
