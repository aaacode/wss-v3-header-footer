
###Vendor Headers and Footers
- see readme file in each club's folder for club-specific documentation
- dynamic header and footer are the custom-element versions
- static header and footer are static html
- both versions reference css files on aaa ftp server
- wssfiles versions are outdated (we previously hosted the css files on the wssfiles ftp server)

###AAA Clubs
- Colorado
- Hoosier
- Minneapolis
- Northway
- Southern Penn
- South Jersey
